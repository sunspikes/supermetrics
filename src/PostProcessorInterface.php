<?php

declare(strict_types=1);

namespace Supermetrics;

use Supermetrics\Entity\Post;

interface PostProcessorInterface
{
    public function process(Post $post): void;

    public function getResult(): array;
}
