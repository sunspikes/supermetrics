<?php

declare(strict_types=1);

namespace Supermetrics\Http;

use DateTimeImmutable;
use GuzzleHttp\ClientInterface as GuzzleHttpClientInterface;
use GuzzleHttp\Promise;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\ResponseInterface;
use Supermetrics\Entity\Post;
use Supermetrics\Exception\PostsClientException;
use Supermetrics\PostsClientInterface;

class PostsHttpClient implements PostsClientInterface
{
    private const POSTS_TOTAL_PAGES = 10;

    /**
     * @var GuzzleHttpClientInterface
     */
    private $httpClient;

    /**
     * @var string
     */
    private $postsEndpoint;

    /**
     * @var string
     */
    private $token;

    public function __construct(GuzzleHttpClientInterface $httpClient, string $postsEndpoint, string $token)
    {
        $this->httpClient    = $httpClient;
        $this->postsEndpoint = $postsEndpoint;
        $this->token         = $token;
    }

    public function getAllPosts(): array
    {
        $posts    = [];
        $promises = [];

        for ($page = 1; $page <= self::POSTS_TOTAL_PAGES; ++$page) {
            $promises[] = $this->httpClient->sendAsync(
                new Request('GET', $this->postsEndpoint . '?' . \http_build_query(['sl_token' => $this->token, 'page' => $page]))
            );
        }

        $results = Promise\settle($promises)->wait();

        foreach ($results as $result) {
            if ($result['state'] !== Promise\PromiseInterface::FULFILLED) {
                throw new PostsClientException('Http client failed, please check the API connection');
            }

            $response = $result['value'];
            if (!$response instanceof ResponseInterface) {
                new PostsClientException('Http client has invalid response');
            }
            if ($response->getStatusCode() !== 200) {
                new PostsClientException('Http client failed with response code: ' . $response->getStatusCode());
            }

            $posts = \array_merge($posts, $this->getPostsFromResponse($response));
        }

        return $posts;
    }

    /**
     * Get a list of Posts from the given response.
     */
    private function getPostsFromResponse(ResponseInterface $response): array
    {
        $posts     = [];
        $body      = \json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
        $postsData = $body['data']['posts'] ?? [];

        foreach ($postsData as $postsDatum) {
            $posts[] = new Post(
                $postsDatum['id'],
                $postsDatum['type'],
                $postsDatum['message'],
                $postsDatum['from_id'],
                $postsDatum['from_name'],
                new DateTimeImmutable($postsDatum['created_time'], new \DateTimeZone('UTC'))
            );
        }

        return $posts;
    }
}
