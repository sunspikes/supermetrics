<?php

declare(strict_types=1);

namespace Supermetrics\Processor;

use Supermetrics\Entity\Post;
use Supermetrics\PostProcessorInterface;

class AverageCharacterLengthProcessor implements PostProcessorInterface
{
    private const CONTEXT_CHARS = 'chars';

    private const CONTEXT_COUNT = 'count';

    private $context = [];

    public function process(Post $post): void
    {
        $month                                      = $post->getCreatedTime()->format('Y-m');
        $this->context[$month][self::CONTEXT_CHARS] = ($this->context[$month][self::CONTEXT_CHARS] ?? 0) + \mb_strlen($post->getMessage());
        $this->context[$month][self::CONTEXT_COUNT] = ($this->context[$month][self::CONTEXT_COUNT] ?? 0) + 1;
    }

    public function getResult(): array
    {
        $result = [];

        foreach ($this->context as $month => $context) {
            $result[$month] = $context[self::CONTEXT_CHARS] / $context[self::CONTEXT_COUNT];
        }

        \ksort($result);

        return $result;
    }
}
