<?php

declare(strict_types=1);

namespace Supermetrics\Processor;

use Supermetrics\Entity\Post;
use Supermetrics\PostProcessorInterface;

class TotalPostsByWeekProcessor implements PostProcessorInterface
{
    private $context = [];

    public function process(Post $post): void
    {
        $weekNumber                 = $post->getCreatedTime()->format('W');
        $this->context[$weekNumber] = ($this->context[$weekNumber] ?? 0) + 1;
    }

    public function getResult(): array
    {
        \ksort($this->context);

        return $this->context;
    }
}
