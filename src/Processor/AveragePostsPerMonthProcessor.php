<?php

declare(strict_types=1);

namespace Supermetrics\Processor;

use Supermetrics\Entity\Post;
use Supermetrics\PostProcessorInterface;

class AveragePostsPerMonthProcessor implements PostProcessorInterface
{
    private $context = [];

    public function process(Post $post): void
    {
        $month                                       = $post->getCreatedTime()->format('Y-m');
        $this->context[$post->getFromName()][$month] = ($this->context[$post->getFromName()][$month] ?? 0) + 1;
    }

    public function getResult(): array
    {
        $result = [];

        foreach ($this->context as $user => $monthly) {
            $result[$user] = \array_sum($monthly) / \count($monthly);
        }

        \ksort($result);

        return $result;
    }
}
