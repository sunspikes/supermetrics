<?php

declare(strict_types=1);

namespace Supermetrics\Processor;

use Supermetrics\Entity\Post;
use Supermetrics\PostProcessorInterface;

class LongestPostPerMonthProcessor implements PostProcessorInterface
{
    private $context = [];

    public function process(Post $post): void
    {
        $month                 = $post->getCreatedTime()->format('Y-m');
        $currentLongest        = ($this->context[$month] ?? 0);
        $newLength             = \mb_strlen($post->getMessage());
        $this->context[$month] = $newLength > $currentLongest ? $newLength : $currentLongest;
    }

    public function getResult(): array
    {
        \ksort($this->context);

        return $this->context;
    }
}
