<?php

declare(strict_types=1);

namespace Supermetrics;

use Supermetrics\Entity\Post;

interface PostsClientInterface
{
    /**
     * Get a list of all posts.
     *
     * @return Post[]
     */
    public function getAllPosts(): array;
}
