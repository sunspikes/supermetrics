<?php

declare(strict_types=1);

namespace Supermetrics\Command;

use Supermetrics\PostsStatsProvider;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PostsStatsCommand extends Command
{
    /**
     * @var PostsStatsProvider
     */
    private $postsStatsProvider;

    public function __construct(PostsStatsProvider $postsStatsProvider)
    {
        parent::__construct();
        $this->postsStatsProvider = $postsStatsProvider;
    }

    /**
     * Configures the command.
     */
    protected function configure(): void
    {
        $this
            ->setName('supermetrics:posts:stats')
            ->setDescription('Get posts stats from supermetrics');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $stats = $this->postsStatsProvider->getPostStats();
            $output->writeln(\json_encode($stats, JSON_PRETTY_PRINT | JSON_THROW_ON_ERROR, 512));
        } catch (\Exception $exception) {
            $output->writeln(\sprintf('<error>Application error: %s</error>', $exception->getMessage()));

            return 1;
        }

        return 0;
    }
}
