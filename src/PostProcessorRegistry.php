<?php

declare(strict_types=1);

namespace Supermetrics;

use Supermetrics\Entity\Post;

class PostProcessorRegistry
{
    /**
     * @var PostProcessorInterface[]
     */
    private $postProcessors;

    public function addPostProcessor(string $name, PostProcessorInterface $postProcessor): void
    {
        $this->postProcessors[$name] = $postProcessor;
    }

    public function process(Post $post): void
    {
        foreach ($this->postProcessors as $name => $postProcessor) {
            $postProcessor->process($post);
        }
    }

    public function getStats(): array
    {
        $stats = [];

        foreach ($this->postProcessors as $name => $postProcessor) {
            $stats[$name] = $postProcessor->getResult();
        }

        return $stats;
    }
}
