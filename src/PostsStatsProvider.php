<?php

declare(strict_types=1);

namespace Supermetrics;

class PostsStatsProvider
{
    /**
     * @var PostsClientInterface
     */
    private $postsClient;

    /**
     * @var PostProcessorRegistry
     */
    private $postProcessorRegistry;

    public function __construct(PostsClientInterface $postsClient, PostProcessorRegistry $postProcessorRegistry)
    {
        $this->postsClient           = $postsClient;
        $this->postProcessorRegistry = $postProcessorRegistry;
    }

    public function getPostStats(): array
    {
        $posts = $this->postsClient->getAllPosts();

        foreach ($posts as $post) {
            $this->postProcessorRegistry->process($post);
        }

        return $this->postProcessorRegistry->getStats();
    }
}
