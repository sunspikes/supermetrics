<?php

declare(strict_types=1);

namespace Supermetrics\Exception;

class PostsClientException extends \RuntimeException
{
}
