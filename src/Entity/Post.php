<?php

declare(strict_types=1);

namespace Supermetrics\Entity;

class Post
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $message;

    /**
     * @var string
     */
    private $fromId;

    /**
     * @var string
     */
    private $fromName;

    /**
     * @var \DateTimeImmutable
     */
    private $createdTime;

    public function __construct(string $id, string $type, string $message, string $fromId, string $fromName, \DateTimeImmutable $createdTime)
    {
        $this->id          = $id;
        $this->type        = $type;
        $this->message     = $message;
        $this->fromId      = $fromId;
        $this->fromName    = $fromName;
        $this->createdTime = $createdTime;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getFromId(): string
    {
        return $this->fromId;
    }

    public function getFromName(): string
    {
        return $this->fromName;
    }

    public function getCreatedTime(): \DateTimeImmutable
    {
        return $this->createdTime;
    }
}
