.PHONY: build
build:
	@docker-compose build

.PHONY: run
run:
	@docker-compose run --rm --no-deps --name supermetrics_app --entrypoint 'sh -c' app 'php app.php supermetrics:posts:stats'

.PHONY: tests
tests:
	@docker-compose run --rm --no-deps --name supermetrics_app --entrypoint 'sh -c' app 'php ./vendor/bin/phpunit -c phpunit.xml.dist'

.PHONY: fix-style
fix-style:
	@docker-compose run --rm --no-deps --name supermetrics_app --entrypoint 'sh -c' app 'php ./vendor/bin/php-cs-fixer fix --config=.php_cs -vvv'
