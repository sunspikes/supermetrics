<?php

declare(strict_types=1);

namespace SupermetricsTests;

use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;
use Supermetrics\Entity\Post;
use Supermetrics\PostProcessorRegistry;
use Supermetrics\PostsClientInterface;
use Supermetrics\PostsStatsProvider;
use Supermetrics\Processor\AverageCharacterLengthProcessor;
use Supermetrics\Processor\AveragePostsPerMonthProcessor;
use Supermetrics\Processor\LongestPostPerMonthProcessor;
use Supermetrics\Processor\TotalPostsByWeekProcessor;

class PostStatsProviderTest extends TestCase
{
    /**
     * @var ObjectProphecy
     */
    private $postsClientProphecy;

    /**
     * @var PostsClientInterface
     */
    private $postsClient;

    public function setUp(): void
    {
        $this->postsClientProphecy = $this->prophesize(PostsClientInterface::class);
        $this->postsClient         = $this->postsClientProphecy->reveal();
    }

    /**
     * @dataProvider dataProvider
     */
    public function testWithData($input, $output): void
    {
        $this->postsClientProphecy
            ->getAllPosts()
            ->willReturn($input);

        $postProcessorRegistry = new PostProcessorRegistry();
        $postProcessorRegistry->addPostProcessor('average_chars', new AverageCharacterLengthProcessor());
        $postProcessorRegistry->addPostProcessor('average_posts', new AveragePostsPerMonthProcessor());
        $postProcessorRegistry->addPostProcessor('longest_post', new LongestPostPerMonthProcessor());
        $postProcessorRegistry->addPostProcessor('total_by_week', new TotalPostsByWeekProcessor());

        $postsStatsProvider = new PostsStatsProvider($this->postsClient, $postProcessorRegistry);

        $this->assertEquals($output, $postsStatsProvider->getPostStats());
    }

    public function dataProvider()
    {
        return [
            [
                [
                    new Post('1', 'post', '123456', 'user_1', 'user_1', new \DateTimeImmutable('2019-09-03T00:00:00')),
                    new Post('2', 'post', '1234567', 'user_2', 'user_2', new \DateTimeImmutable('2019-09-10T00:00:00')),
                    new Post('3', 'post', '12345678', 'user_3', 'user_3', new \DateTimeImmutable('2019-09-17T00:00:00')),
                    new Post('4', 'post', '123456789', 'user_4', 'user_4', new \DateTimeImmutable('2019-09-24T00:00:00')),
                    new Post('5', 'post', '1', 'user_1', 'user_1', new \DateTimeImmutable('2019-10-01T00:00:00')),
                    new Post('6', 'post', '12', 'user_2', 'user_2', new \DateTimeImmutable('2019-10-08T00:00:00')),
                    new Post('7', 'post', '123', 'user_3', 'user_3', new \DateTimeImmutable('2019-10-15T00:00:00')),
                    new Post('8', 'post', '1234', 'user_4', 'user_4', new \DateTimeImmutable('2019-10-22T00:00:00')),
                    new Post('9', 'post', '12345', 'user_4', 'user_4', new \DateTimeImmutable('2019-10-23T00:00:00')),
                ],
                [
                    'average_chars' => [
                        '2019-09' => 7.5,
                        '2019-10' => 3,
                    ],
                    'average_posts' => [
                        'user_1' => 1,
                        'user_2' => 1,
                        'user_3' => 1,
                        'user_4' => 1.5,
                    ],
                    'longest_post' => [
                        '2019-09' => 9,
                        '2019-10' => 5,
                    ],
                    'total_by_week' => [
                        '36' => 1,
                        '37' => 1,
                        '38' => 1,
                        '39' => 1,
                        '40' => 1,
                        '41' => 1,
                        '42' => 1,
                        '43' => 2,
                    ],
                ],
            ],
        ];
    }
}
