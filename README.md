## Supermetrics posts stats application

This application fetches posts from the supermetrics API and displays the following stats

 - Average character length of posts per month
 - Longest post by character length per month
 - Total posts split by week for the whole data
 - Average number of posts for each users per month
 
### Setup

To setup and run the application

1. Update the `API_TOKEN` in the `.env` file
2. Run `make build` - This will build the docker image and install the composer deps
3. Run `make run` - This will run the application and display the stats in `json` format

### Tests

Tests are in the `tests` directory, to run tests, run

`make tests`


### Example

```
$ make run                                                                                                                                            7s < Wed Nov 27 22:57:38 2019
{
    "Average character length of a post per month": {
        "2019-05": 334.14285714285717,
        "2019-06": 399.06962025316454,
        "2019-07": 377.6666666666667,
        "2019-08": 381.9756097560976,
        "2019-09": 381.4691358024691,
        "2019-10": 388.8633540372671,
        "2019-11": 408.78985507246375
    },
    "Longest post by character length per month": {
        "2019-05": 781,
        "2019-06": 775,
        "2019-07": 752,
        "2019-08": 762,
        "2019-09": 745,
        "2019-10": 750,
        "2019-11": 742
    },
    "Total posts split by week": {
        "21": 24,
        "22": 36,
        "23": 37,
        "24": 36,
        "25": 37,
        "26": 37,
        "27": 39,
        "28": 38,
        "29": 35,
        "30": 39,
        "31": 38,
        "32": 38,
        "33": 37,
        "34": 37,
        "35": 35,
        "36": 39,
        "37": 39,
        "38": 38,
        "39": 37,
        "40": 36,
        "41": 38,
        "42": 36,
        "43": 37,
        "44": 33,
        "45": 37,
        "46": 37,
        "47": 38,
        "48": 12
    },
    "Average number of posts per user per month": {
        "Britany Heise": 7.857142857142857,
        "Carly Alvarez": 6.142857142857143,
        "Carson Smithson": 7.428571428571429,
        "Ethelene Maggi": 6.714285714285714,
        "Filomena Cort": 5.714285714285714,
        "Gigi Richter": 7.714285714285714,
        "Isidro Schuett": 5.857142857142857,
        "Lael Vassel": 10,
        "Lashanda Small": 6.428571428571429,
        "Leonarda Schult": 7.142857142857143,
        "Macie Mckamey": 7,
        "Mandie Nagao": 6.857142857142857,
        "Maxie Marceau": 6.714285714285714,
        "Nydia Croff": 9,
        "Quyen Pellegrini": 6.857142857142857,
        "Rafael Althoff": 7.571428571428571,
        "Regenia Boice": 5,
        "Rosann Eide": 8.142857142857142,
        "Woodrow Lindholm": 8,
        "Yolande Urrutia": 8.142857142857142
    }
}
```
