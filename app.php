<?php

declare(strict_types=1);

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/container.php';

use Supermetrics\Command\PostsStatsCommand;
use Symfony\Component\Console\Application;

$application = new Application('Supermetrics posts stats application');
$application->add($container[PostsStatsCommand::class]);
$application->run();
