<?php

declare(strict_types=1);

use GuzzleHttp\Client as GuzzleHtttpClient;
use Pimple\Container;
use Supermetrics\Command\PostsStatsCommand;
use Supermetrics\Http\PostsHttpClient;
use Supermetrics\PostProcessorRegistry;
use Supermetrics\PostsStatsProvider;
use Supermetrics\Processor\AverageCharacterLengthProcessor;
use Supermetrics\Processor\AveragePostsPerMonthProcessor;
use Supermetrics\Processor\LongestPostPerMonthProcessor;
use Supermetrics\Processor\TotalPostsByWeekProcessor;
use Symfony\Component\Dotenv\Dotenv;

(new Dotenv())->load(__DIR__ . '/.env');

$container = new Container();

$container[PostsHttpClient::class] = function (Container $container) {
    return new PostsHttpClient(new GuzzleHtttpClient(), $_ENV['API_POSTS_ENDPOINT'], $_ENV['API_TOKEN']);
};

$container[PostProcessorRegistry::class] = function (Container $container) {
    $postProcessorRegistry = new PostProcessorRegistry();
    $postProcessorRegistry->addPostProcessor('Average character length of a post per month', new AverageCharacterLengthProcessor());
    $postProcessorRegistry->addPostProcessor('Longest post by character length per month', new LongestPostPerMonthProcessor());
    $postProcessorRegistry->addPostProcessor('Total posts split by week', new TotalPostsByWeekProcessor());
    $postProcessorRegistry->addPostProcessor('Average number of posts per user per month', new AveragePostsPerMonthProcessor());
    return $postProcessorRegistry;
};

$container[PostsStatsProvider::class] = function (Container $container) {
    return new PostsStatsProvider($container[PostsHttpClient::class], $container[PostProcessorRegistry::class]);
};

$container[PostsStatsCommand::class] = function (Container $container) {
    return new PostsStatsCommand($container[PostsStatsProvider::class]);
};
